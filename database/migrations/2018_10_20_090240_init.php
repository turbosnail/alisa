<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Init extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('countries', function ($table)
        {
            $table->increments('id');
            $table->string('code');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
            

        });

        Schema::create('regions', function ($table)
        {
            $table->increments('id');
            $table->string('code');
            $table->string('country_id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('settlements', function ($table)
        {
            $table->increments('id');
            $table->string('code');
            $table->integer('country_id');
            $table->string('region_id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('stations', function ($table)
        {
            $table->increments('id');
            $table->string('code');
            $table->integer('country_id');
            $table->integer('region_id');
            $table->integer('settlement_id');
            $table->string('name');
            $table->string('direction');
            $table->string('station_type');
            $table->string('transport_type');
            $table->float('latitude');
            $table->float('longitude');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
        Schema::drop('regions');
        Schema::drop('settlements');
        Schema::drop('stations');
    }
}
