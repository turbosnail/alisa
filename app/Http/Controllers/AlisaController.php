<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AlisaController extends Controller
{
    protected $resposne = [];

    public function __construct()
    {
        
        $this->data = [
            'response' => [
                'text' => '',
                'tts' => '',
            ],
            'version' => '1.0',
            'session' => [
                'session_id' => '',
                'message_id' => '',
                'user_id' => '',
            ],
        ];
    }
    //
    public function __invoke()
    {
        $this->prepareResponse();
        $this->handleRequest();
        return $this->data;
    }

    public function prepareResponse()
    {
        $this->data['session'] = [
            'session_id' => request('session')['session_id'],
            'message_id' => request('session')['message_id'],
            'user_id' => request('session')['user_id'],
        ];
    }

    public function handleRequest()
    {
        # code...
    }
}
