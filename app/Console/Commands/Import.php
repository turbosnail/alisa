<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \App\Models\Country;
use \App\Models\Region;
use \App\Models\Settlement;
use \App\Models\Station;


class Import extends Command
{
    /**
     * Api key for yandex
     * 
     * @var string
     */
    protected $apikey = '';

    /**
     * Список всех доступных станций
     */
    protected $stationsUrl = 'https://api.rasp.yandex.net/v3.0/stations_list/';

    /**
     * Расписание рейсов по станции
     */
    protected $scheduleUrl = 'https://api.rasp.yandex.net/v3.0/schedule/';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from rasp.yandex.ru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->apikey = env('YANDEX_API_KEY');
        ini_set('memory_limit', '2G');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        if (empty($this->apikey)) {
            $this->error('Invalid yandex api key');
            return;
        }
        

        // $file = base_path() . '/rasp.json';

        // if (!file_exists($file)) {
        //     $this->error('rasp.json');
        //     return;
        // }

        $json = json_decode(
            file_get_contents(
                $this->stationsUrl . '?apikey=' . $this->apikey
            ), 
            true
        );

        if(!is_array($json)) {
            $this->error('response in not a json');
            return;
        }

        foreach ($json['countries'] as $country) {
            $obCountry = Country::query()
            ->where('code', $country['codes']['yandex_code'])
            ->first();

            if (null === $obCountry) {
                $obCountry = new Country();
                $obCountry->code = $country['codes']['yandex_code'];
            }
            
            $obCountry->name = $country['title'];
            $obCountry->save();
            
            foreach ($country['regions'] as $region) {
                $obRegion = null;

                if (!empty($region['codes']['yandex_code'])) {
                    $obRegion = Region::query()
                    ->where('code', $region['codes']['yandex_code'])
                    ->first();

                    if (null === $obRegion) {
                        $obRegion = new Region();
                        $obRegion->code = $region['codes']['yandex_code'];
                    }

                    $obRegion->country_id = $obCountry->id;
                    $obRegion->name = $region['title'];
                    $obRegion->save();
                }

                foreach ($region['settlements'] as $settlement) {
                    $obSettlement = null;

                    if (!empty($settlement['codes']['yandex_code'])) {
                        $obSettlement = Settlement::query()
                        ->where('code', $settlement['codes']['yandex_code'])
                        ->first();
    
                        if (null === $obSettlement) {
                            $obSettlement = new Settlement();
                            $obSettlement->code = $settlement['codes']['yandex_code'];
                        }

                        if (null === $obRegion) {
                            $obSettlement->region_id = 0;
                        } else {
                            $obSettlement->region_id = $obRegion->id;
                        }
    
                        $obSettlement->country_id = $obCountry->id;
                        $obSettlement->name = $settlement['title'];
                        $obSettlement->save();
                    }

                    foreach ($settlement['stations'] as $station) {
                        $obStation = Station::query()
                        ->where('code', $station['codes']['yandex_code'])
                        ->first();

                        if (null === $obStation) {
                            $obStation = new Station();
                            $obStation->code = $station['codes']['yandex_code'];
                        }

                        if (null === $obRegion) {
                            $obStation->region_id = 0;
                        } else {
                            $obStation->region_id = $obRegion->id;
                        }

                        if (null === $obSettlement) {
                            $obStation->settlement_id = 0;
                        } else {
                            $obStation->settlement_id = $obSettlement->id;
                        }

                        $obStation->country_id = $obCountry->id;
                        $obStation->name = $station['title'];
                        $obStation->direction = $station['direction'];
                        $obStation->station_type = $station['station_type'];
                        $obStation->transport_type = $station['transport_type'];
                        $obStation->latitude = $station['latitude'];
                        $obStation->longitude = $station['longitude'];
                        $obStation->save();
                    }
                }
                
            }

            
        }
        // print_r(array_keys($json['countries'][0]
        // ['regions'][0]
        // ['settlements'][0]
        // ['stations'][0]
        // ));
        // $this->($this->apikey);
    }
}
